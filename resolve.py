import json
import psycopg2
import psycopg2.extras
from postgis.psycopg import register
from postgis import Point

propertyPaths = {
  "Event": {
    "longitude":["location","geo","longitude"],
    "latitude":["location","geo","latitude"]
  },
  "Place": {
    "longitude":["geo","longitude"],
    "latitude":["geo","latitude"]
  }
}

def resolve(object, property):
  try:
    path = propertyPaths[object["@type"]][property]
    for property in path:
      object = object[property]
    return object
  except KeyError:
    return None

data = [
    'flaneur.jsonl',
    'tattoo.jsonl',
    'facebook-places-bars.jsonl'
]

items = []

for d in data:
    with open(d) as f:
        for line in f:
            items.append(json.loads(line))


con = psycopg2.connect(host='localhost', user='hackathon', password='hackathon', database='hackathon')
register(con)
psycopg2.extensions.register_adapter(dict, psycopg2.extras.Json)
cur = con.cursor()


for item in items:
    if item["@type"] == "Place" or item["@type"] == "Event":
        longitude = resolve(item, "longitude")
        latitude = resolve(item, "latitude")
        if longitude and latitude:
            print(item["@type"] + ", long: " + str(resolve(item, "longitude")) +", lat: " + str(resolve(item, "latitude")) + ", name: "+ item["name"])
            cur.execute('''insert into entities (type, name, coord, data) values (%s, %s,
                    ST_Point(%s,%s), %s)''',(
                item['@type'],
                item['name'],
                longitude, latitude,
                item))

con.commit()
