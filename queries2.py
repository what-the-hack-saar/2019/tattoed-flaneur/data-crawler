import psycopg2
import psycopg2.extras
from postgis.psycopg import register
from postgis import Point
import json

#   connect to database
con = psycopg2.connect(host='68.183.222.137', user='hackathon', password='hackathon', database='hackathon')
register(con)
psycopg2.extensions.register_adapter(dict, psycopg2.extras.Json)
cur = con.cursor()

cur.execute("""
    with locs as (
        select ST_Centroid((select ST_Collect(coord) from coordinates where id in (37,39,41))) as c
    ),
    x as (
        select
            p.osm_id, ST_DISTANCE((select locs.c from locs), p.way) as d
        from planet_osm_point as p
            where p.amenity in ('bar', 'pub', 'restaurant', 'biergarten', 'cafe', 'shop')
            order by ST_DISTANCE((select locs.c from locs), p.way)
            limit 10
    )
    select x.osm_id, x.d, p.name, ST_AsGeoJSON(p.way) from x join planet_osm_point as p on x.osm_id=p.osm_id
    """)

result =  [{'name': name, 'id': id, 'geo': geo} for id, _, name, geo in cur.fetchall()]

with open('near_by.json','w') as o:
    json.dump(result, o)



"""
        select osm_id, min(ST_DISTANCE(locs.coord, way)) as d 
            from locs, planet_osm_point as p 
            where p.amenity in ('bar', 'pub', 'restaurant', 'biergarten', 'cafe', 'shop')
            group by osm_id
            order by min(ST_DISTANCE(locs.coord, way))
            limit 30
"""
