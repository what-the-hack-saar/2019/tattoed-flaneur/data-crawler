import psycopg2
import psycopg2.extras
from postgis.psycopg import register
from postgis import Point

#   connect to database
con = psycopg2.connect(host='68.183.222.137', user='hackathon', password='hackathon', database='hackathon')
register(con)
psycopg2.extensions.register_adapter(dict, psycopg2.extras.Json)
cur = con.cursor()

cur.execute("""
    with locs as (
       select id, coord from coordinates where id in (37,39,41)
    ),
    dists as (
        select id, c.osm_id, c.d from locs,
            lateral (
                select osm_id, ST_DISTANCE(locs.coord, way) as d from planet_osm_point as p
                    where p.amenity in ('bar', 'pub', 'restaurant', 'biergarten', 'cafe', 'shop')
                    order by ST_DISTANCE(locs.coord, way) asc
                    limit 5
                ) as c
            ),
    x as (
        select
            p.osm_id, sum(ST_DISTANCE(c.coord, p.way)) as d
        from coordinates as c, planet_osm_point as p
            where
                p.osm_id in (select osm_id from dists)
                and
                c.id in (select id from locs)
            group by p.osm_id
            order by sum(ST_DISTANCE(c.coord, p.way))
            limit 10
    )
    select x.osm_id, x.d, p.name from x join planet_osm_point as p on x.osm_id=p.osm_id
    """)

for id, dist, name in cur.fetchall():
    print(id, dist, name)



"""
        select osm_id, min(ST_DISTANCE(locs.coord, way)) as d 
            from locs, planet_osm_point as p 
            where p.amenity in ('bar', 'pub', 'restaurant', 'biergarten', 'cafe', 'shop')
            group by osm_id
            order by min(ST_DISTANCE(locs.coord, way))
            limit 30
"""
