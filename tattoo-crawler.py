import requests
from io import BytesIO
from lxml import etree
from bs4 import BeautifulSoup
import json

def get_urls():
    html = requests.get('https://tattoo.saarland/job_listing-sitemap.xml')
    tree = etree.fromstring(html.content)

    for x in tree.xpath('//*[local-name() = "loc"]'):
        url = x.text
        if url.endswith('/'):
            yield url



with open('tattoo.jsonl', 'w') as out:
    for x in get_urls():
        print(x)
        details = requests.get(x)
        soup = BeautifulSoup(details.content, 'html.parser')
        for y in soup.find_all("script", attrs={'type':'application/ld+json'}):
            print(y.text)
            data = json.loads(y.text)
            json.dump(data, out)
            out.write('\n')

