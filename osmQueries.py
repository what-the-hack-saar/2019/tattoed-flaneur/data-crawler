import psycopg2
import psycopg2.extras
import json

#   connect to database
con = psycopg2.connect(host='68.183.222.137', user='hackathon', password='hackathon', database='hackathon')

cur = con.cursor()

cur.execute("select n.lat, n.lon, p.name, n.tags from planet_osm_nodes as n, planet_osm_point as p where p.amenity in ('bar', 'pub', 'restaurant', 'biergarten', 'cafe', 'shop') and n.id = p.osm_id;")
results = cur.fetchall()

def mapProperty(target, tags, src, dest):
  if tags.get(src) != None:
    target[dest] = tags[src]

def mapAll(target, tags, mapping):
  for src in mapping:
    mapProperty(target, tags, src, mapping[src])

mappingsPlace = {
  "description": "description",
  "description:de": "description",
  "phone": "telephone"
}

for result in results:
  # print(result)
  tags = {}
  nextTag = None
  for tagOrValue in result[3]:
    if nextTag == None:
      nextTag = tagOrValue
    else:
      tags[nextTag] = tagOrValue
      nextTag = None
  place = {
    "@context": "http://schema.org",
    "@type": "Place",
    "name" : result[2]
  }
  geo = {
    "@type":"GeoCoordinates",
    "latitude": result[0],
    "longitude": result[1]
  }
  place["geo"] = geo
  if tags.get("website")!=None:
    url = {
      "@type": "URL",
      "@id": tags["website"]
    }
    place["url"] = url
  if tags.get("addr:city")!=None and tags.get("addr:postcode")!=None and tags.get("addr:street")!=None and tags.get("addr:housenumber")!=None:
    address = {
      "@type": "PostalAdress",
      "addressLocality": tags["addr:city"],
      "postalCode": tags["addr:postcode"],
      "streetAddress": tags["addr:street"] + " " + tags["addr:housenumber"]
    }
    place["address"] = address
  mapAll(place, tags, mappingsPlace)
  print(json.dumps(place))
