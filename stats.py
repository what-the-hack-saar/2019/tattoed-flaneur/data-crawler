import json
from itertools import chain
from collections import Counter

data = [
    'flaneur.jsonl',
    'tattoo.jsonl'
]

items = []

for d in data:
    with open(d) as f:
        for line in f:
            items.append(json.loads(line))

print(len(items))

c = Counter([i['@type'] for i in items])

print(c)

c = Counter(chain(*[i.keys() for i in items]))
print(c)
